# COMP6551 study group

each worksheet has its own folder, you can create files inside that folder.
In general its better to write in a textual format rather than upload binary filed (pdf, png, ect.)
Github mainly supports [Markdown] as its textual notation.
Gitlab also added mathematical notations using [Latex]syntax.


[Markdown]: https://docs.gitlab.com/ee/user/markdown.html
[Latex]: https://oeis.org/wiki/List_of_LaTeX_mathematical_symbols

## Worksheets
* [Worksheet1: mathematical preliminaries](ws1/)
* [Worksheet2: Divide and Conquer](ws2/)
* [Worksheet3: Greedy algorithms](ws3/)
* [Worksheet4: Dynamic Programming](ws4/)
* [Worksheet5: Graph algorithms](ws5/)


## Example

See [ws2/question 5](ws2/q5.md) to see an example of markdown formated solution.